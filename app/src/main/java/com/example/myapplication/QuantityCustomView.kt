package com.example.myapplication

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout

class QuantityCustomView(context:Context, attributeSet: AttributeSet):ConstraintLayout(context,attributeSet) {
    private var defaultValue = 0
    private var displayMinus = true
    lateinit var paint: Paint
    init{
        setWillNotDraw(false)
        setAttributes(attributeSet)
     initView()
    }

    private fun setAttributes(attributeSet: AttributeSet) {
        val attributes = context.theme.obtainStyledAttributes(attributeSet,R.styleable.QuantityCustomView,0,0)
        defaultValue = attributes.getInt(R.styleable.QuantityCustomView_defaultQuantityValue,0)
        displayMinus = attributes.getBoolean(R.styleable.QuantityCustomView_displayMinusButton,true)

    }

    private fun initView() {
        inflate(context,R.layout.quantity_custom_view,this)
        val edt:EditText = findViewById(R.id.edt)
        edt.setText("$defaultValue")

        val btnPlus: Button = findViewById(R.id.btnPlus)
        btnPlus.setOnClickListener { defaultValue++
            edt.setText("$defaultValue")
        }

        val btnMinus: Button = findViewById(R.id.btnMinus)
        btnMinus.setOnClickListener { defaultValue--
            edt.setText("$defaultValue")
        }

        if(!displayMinus){
            btnMinus.visibility = View.GONE
        }

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint = Paint()
        paint.color = Color.BLUE
        paint.style = Paint.Style.FILL
        canvas?.drawCircle(100F,400F,40F,paint)
    }
}